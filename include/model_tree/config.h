#pragma once

#include <string>
#include <vector>

#include <Eigen/Core>

#include "sr/io/versioned_config.h"
#include "sr/reflection/struct.h"
#include "sr/util/const_value.h"

#include "errordetection_types/element_type.h"

namespace model_parser::model_tree
{

template <int Version = 1>
struct ModelTreeConfig;

template <>
struct ModelTreeConfig<0> : sr::io::VersionedConfig
{
    sr::util::ConstValue<int> version{ 0 };
    std::unordered_map<std::string, std::string> namingL0Map;
};

template <>
struct ModelTreeConfig<1> : sr::io::VersionedConfig
{
    sr::util::ConstValue<int> version{ 1 };
    std::unordered_map<std::string, std::string> namingL0Map;
    std::string delimitersL1;
};

// Define version migration methods
void migrateConfig(const ModelTreeConfig<0>& from, ModelTreeConfig<1>& to)
{
    // Config version 1 adds support for custom delimiters

    to.namingL0Map = from.namingL0Map;
    to.delimitersL1 = ":";
}

} // namespace model_parser::model_tree

// clang-format off
SR_REFLECTION_REGISTER_STRUCT(model_parser::model_tree::ModelTreeConfig<0>, "ModelTreeConfig",
    (version,              "version",                 "Config version 0")
    (namingL0Map,          "namingL0Map",             "Map relating IFC names with model tree L0 labels.")
)

SR_REFLECTION_REGISTER_STRUCT(model_parser::model_tree::ModelTreeConfig<1>, "ModelTreeConfig",
    (version,              "version",                 "Config version 0")
    (namingL0Map,          "namingL0Map",             "Map relating IFC names with model tree L0 labels.")
    (delimitersL1,         "delimitersL1",            "String delimiters to split the object name.")
)
// clang-format on
