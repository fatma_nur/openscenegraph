/*
 * Copyright (c) 2020, Scaled Robotics SL
 * License : http://www.binpress.com/license/view/l/8ef75db882b3f91ba42b114142f3e8b3
 */

/** \file */

#pragma once

#include <iostream>
#include <string>

#include "sr/model/guid.h"
#include "sr/model/mesh.h"

#include "errordetection_types/element_metadata.h"

#include "ifc_parser/element.h"
#include "ifc_parser/ifc_file.h"

namespace model_parser::ifc_parser
{
class AssemblyElements
{

public:
    inline AssemblyElements();

    inline AssemblyElements(const errordetection_types::ElementMetadata& t_metadata);

    inline AssemblyElements(const sr::model::GUID& guid);

    inline sr::model::GUID getGUID() const;

    inline sr::model::Mesh<double, 3, 2> getGeometry() const;

    inline errordetection_types::ElementMetadata getMetadata() const;

    inline void addElement(const model_parser::ifc_parser::Element& element);

    inline std::vector<model_parser::ifc_parser::Element>& getElements();

private:
    sr::model::GUID m_guid;                                    /**< Assembly GUID */
    errordetection_types::ElementMetadata m_metadata;          /**< Assembly metadata */
    std::vector<model_parser::ifc_parser::Element> m_elements; /**< Elements that belong to this assembly */
};

/**
 * Merges all assembly elements from a set of input IFC files
 * \param[in] inputFiles vector of input files
 * \param[in] elementList map of elements metadata
 * \return merged map of elements metadata
 */
inline std::map<sr::model::GUID, model_parser::ifc_parser::AssemblyElements> mergeAssemblyElements(
  const std::vector<boost::filesystem::path>& inputFiles,
  std::map<sr::model::GUID, model_parser::ifc_parser::AssemblyElements>& elementList);

} // namespace model_parser::ifc_parser::

#include "ifc_parser/assembly_elements.hpp"
