#pragma once

#include <string>
#include <vector>

#include <Eigen/Core>

#include "sr/io/versioned_config.h"
#include "sr/reflection/struct.h"
#include "sr/util/const_value.h"

namespace model_parser::ifc_parser
{

template <int Version = 1>
struct IfcParserConfig;

template <>
struct IfcParserConfig<0> : sr::io::VersionedConfig
{
    sr::util::ConstValue<int> version{ 0 };
    bool filterTransparent = false;
    double transparentThreshold = 0.7;
    double minDiameterThreshold = 0.05;
    double minAreaThreshold = 0.01;
    std::string geometryFormat = "obj";
    bool filterElements = true;
    std::vector<std::string> elementsToFilter{ "IfcOpeningElement",
                                               "IfcReinforcingBar",
                                               "IfcReinforcingMesh" };
    boost::optional<Eigen::Matrix<double, 3, 1>> translation;
};

template <>
struct IfcParserConfig<1> : sr::io::VersionedConfig
{
    sr::util::ConstValue<int> version{ 1 };
    bool filterTransparent = false;
    double transparentThreshold = 0.7;
    double minDiameterThreshold = 0.05;
    double minAreaThreshold = 0.01;
    std::string geometryFormat = "obj";
    bool filterElements = true;
    std::vector<std::string> elementsToFilter{ "IfcOpeningElement",
                                               "IfcReinforcingBar",
                                               "IfcReinforcingMesh" };
    boost::optional<Eigen::Matrix<double, 3, 1>> translation;
    bool mergeAssemblies = false;
};

// Define version migration predicates
void migrateConfig(const IfcParserConfig<0>& from, IfcParserConfig<1>& to)
{
    // Config version 1 introduces mergeAssemblies flag
    to.filterTransparent = from.filterTransparent;
    to.transparentThreshold = from.transparentThreshold;
    to.minDiameterThreshold = from.minDiameterThreshold;
    to.minAreaThreshold = from.minAreaThreshold;
    to.geometryFormat = from.geometryFormat;
    to.filterElements = from.filterElements;
    to.elementsToFilter = from.elementsToFilter;
    to.translation = from.translation;
}

} // namespace model_parser::ifc_parser

// clang-format off
SR_REFLECTION_REGISTER_STRUCT(model_parser::ifc_parser::IfcParserConfig<0>, "IfcParserConfig",
    (version,              "version",                 "Config version 0")
    (filterTransparent,    "filterTransparent",       "Filter transparent elements")
    (transparentThreshold, "transparentThreshold",    "Transparent threshold")
    (minDiameterThreshold, "minDiameterThreshold",    "Minimum element diameter of elements")
    (minAreaThreshold,     "minAreaThreshold",        "Minimum surface area of elements")
    (geometryFormat,       "geometryFormat",          "Format of the output geometry")
    (filterElements,       "filterElements",          "Apply a filter to the elements")
    (elementsToFilter,     "elementsToFilter",        "List of element classes to be filtered out")
    (translation,          "translation",             "Apply a translation to the generated geometries")
)

SR_REFLECTION_REGISTER_STRUCT(model_parser::ifc_parser::IfcParserConfig<1>, "IfcParserConfig",
    (version,              "version",                 "Config version 1")
    (filterTransparent,    "filterTransparent",       "Filter transparent elements")
    (transparentThreshold, "transparentThreshold",    "Transparent threshold")
    (minDiameterThreshold, "minDiameterThreshold",    "Minimum element diameter of elements")
    (minAreaThreshold,     "minAreaThreshold",        "Minimum surface area of elements")
    (geometryFormat,       "geometryFormat",          "Format of the output geometry")
    (filterElements,       "filterElements",          "Apply a filter to the elements")
    (elementsToFilter,     "elementsToFilter",        "List of element classes to be filtered out")
    (translation,          "translation",             "Apply a translation to the generated geometries")
    (mergeAssemblies,      "mergeAssemblies",         "Merge elements grouped by IfcAssemblyElements into one geometry & metadata")
)
// clang-format on
