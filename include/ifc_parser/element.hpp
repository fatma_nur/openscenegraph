#pragma once

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/connected_components.hpp>
#include <boost/property_map/property_map.hpp>

#include "sr/logging/log_writer.h"
#include "sr/model/geometry.h"

#include "ifc_parser/element.h"

inline model_parser::ifc_parser::Element::Element(
  std::string t_type,
  sr::model::GUID t_guid,
  sr::model::Mesh<double, 3, 2>& t_geometry)
{
    this->m_elementType = t_type;
    this->m_GUID = t_guid;
    this->m_elementName = this->m_elementType + "-" + this->m_GUID.string();
    sr::logging::debug("Initializing Element {}", this->m_elementName);
    this->m_geometry = t_geometry;

    this->m_metadata.guid = t_guid;
    this->m_metadata.elementType = t_type;
    this->m_metadata.bbox = this->getBoundingBox(this->m_geometry);
}

inline sr::model::GUID model_parser::ifc_parser::Element::getGUID() const
{
    return this->m_GUID;
}

inline std::string model_parser::ifc_parser::Element::getElementType() const
{
    return this->m_elementType;
}

inline std::string model_parser::ifc_parser::Element::getElementName() const
{
    return this->m_elementName;
}

const sr::model::Mesh<double, 3, 2>& model_parser::ifc_parser::Element::getGeometry() const
{
    return this->m_geometry;
}

inline errordetection_types::ElementMetadata& model_parser::ifc_parser::Element::getMetadata()
{
    return this->m_metadata;
}

inline errordetection_types::ElementMetadata model_parser::ifc_parser::Element::getMetadata() const
{
    return this->m_metadata;
}

inline std::array<double, 6> model_parser::ifc_parser::Element::getBoundingBox(
  sr::model::Mesh<double, 3, 2>& model) const
{
    sr::math::Box3d bbox = sr::model::boundingBox(model);

    Eigen::Vector3d minPoint = bbox.minVertex();
    Eigen::Vector3d maxPoint = bbox.maxVertex();
    return std::array<double, 6>{ minPoint.x(), maxPoint.x(), minPoint.y(),
                                  maxPoint.y(), minPoint.z(), maxPoint.z() };
}
