/*
 * Copyright (c) 2020, Scaled Robotics SL
 * License : http://www.binpress.com/license/view/l/8ef75db882b3f91ba42b114142f3e8b3
 */

/** \file */

#pragma once

#include <fmt/format.h>

#include "ifc_parser/assembly.h"

inline model_parser::ifc_parser::Assembly::Assembly(const errordetection_types::ElementMetadata t_metadata)
{
    this->m_metadata = t_metadata;
}

inline sr::model::GUID model_parser::ifc_parser::Assembly::getGUID() const
{
    return this->m_metadata.guid;
}

inline const errordetection_types::ElementMetadata& model_parser::ifc_parser::Assembly::getMetadata() const
{
    return this->m_metadata;
}

inline errordetection_types::ElementMetadata& model_parser::ifc_parser::Assembly::getMetadata()
{
    return this->m_metadata;
}

inline void model_parser::ifc_parser::Assembly::addElement(const sr::model::GUID& guid)
{
    this->m_elementGUIDs.push_back(guid);
}

inline std::vector<sr::model::GUID>& model_parser::ifc_parser::Assembly::getElements()
{
    return this->m_elementGUIDs;
}

inline const std::vector<sr::model::GUID>& model_parser::ifc_parser::Assembly::getElements() const
{
    return this->m_elementGUIDs;
}