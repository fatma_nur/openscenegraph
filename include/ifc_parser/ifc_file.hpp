#pragma once

#include <boost/make_shared.hpp>

#include "sr/cloud/cloud_analysis.h"
#include "sr/logging/log_writer.h"
#include "sr/model/geometry.h"
#include "sr/visualization/utils.h"

#include "ifc_parser/ifc_file.h"

namespace model_parser::ifc_parser
{

using namespace IfcSchema;

IfcFile::IfcFile(boost::filesystem::path t_file, IfcFileFilters filters)
{
    this->m_filePath = t_file;
    this->m_file = boost::make_shared<IfcParse::IfcFile>();
    this->m_elementsDone = false;
    this->m_progress = -1;
    this->m_nAssignedElements = 0;
    this->m_filters = filters;
    this->createFilters();
}

inline IfcFile::IfcFile(const IfcFile& t_other)
{
    this->m_filePath = t_other.getFilePath();
    this->m_file = t_other.m_file;
    this->m_filters = t_other.m_filters;
    this->m_elementsDone = t_other.getElementsDone();
    this->m_progress = t_other.getProgress();
    this->m_nAssignedElements = t_other.getNumberOfAssignedElements();
    this->m_IFCfilters = t_other.m_IFCfilters;
}

boost::filesystem::path IfcFile::getFilePath() const
{
    return this->m_filePath;
}

int IfcFile::getProgress() const
{
    return this->m_progress;
}

std::size_t IfcFile::getIdxShard() const
{
    return this->m_filters.shard;
}

std::size_t IfcFile::getNumShards() const
{
    return this->m_filters.nThreads;
}

bool IfcFile::getElementsDone() const
{
    return this->m_elementsDone;
}

size_t IfcFile::getNumberOfAssignedElements() const
{
    return this->m_nAssignedElements;
}

bool IfcFile::read()
{
    sr::logging::info("Reading IFC file {}", this->m_filePath.string());

    if (this->m_filters.nThreads != 1)
    {
        sr::logging::info("thread {}/{}", 1 + this->m_filters.shard, this->m_filters.nThreads);
    }

    if (!boost::filesystem::exists(this->m_filePath))
    {
        throw std::invalid_argument(fmt::format("IFC file {} does not exist", this->m_filePath.string()));
    }
    if (this->m_filePath.extension().string() != ".ifc")
    {
        throw std::invalid_argument(fmt::format("{} is not an IFC file.", this->m_filePath.string()));
    }

    bool fileOpen = m_file->Init(m_filePath.string());
    IfcRoot::list::ptr entities = m_file->entitiesByType<IfcRoot>();

    fileOpen = fileOpen && (entities->begin() != entities->end());
    if (!fileOpen)
    {
        return fileOpen;
    }

    sr::logging::info("IFC file read.");

    this->m_progress = 0.0;
    return fileOpen;
}

void IfcFile::populateElements(std::vector<model_parser::ifc_parser::Element>& t_elements)
{
    sr::logging::debug("Looking for geometrical entitites.");

    // Get iterator settings
    IfcGeom::IteratorSettings settings;
    settings.set(IfcGeom::IteratorSettings::USE_WORLD_COORDS, true);

    IfcProduct::list::ptr products = m_file->entitiesByType<IfcProduct>();
    for (const auto& product : *products)
    {
        std::hash<std::string> hash;
        if (hash(product->GlobalId()) % this->m_filters.nThreads == this->m_filters.shard)
        {
            this->m_nAssignedElements++;
        }
    }

    // Start iterator. Elements filtered by this->m_IFCfilters won't be computed.
    IfcGeom::Iterator<double> contextIterator(settings, m_file.get(), this->m_IFCfilters);

    if (!contextIterator.initialize())
    {
        sr::logging::debug("No geometrical entities found. Initialization failed.");
        if (this->m_filters.nThreads != 1)
        {
            sr::logging::debug("thread {}/{}", 1 + this->m_filters.shard, this->m_filters.nThreads);
        }
        this->m_elementsDone = true;
        this->m_progress = 100.0;
        return;
    }
    this->m_progress = 0.0;
    int progress, n;
    n = 0;
    do
    {
        n++;
        progress = contextIterator.progress();
        IfcGeom::Element<double>* geomObject = contextIterator.get();
        std::string elementName = geomObject->type() + "-" + geomObject->guid();

        sr::model::GUID entityGUID(geomObject->guid());
        sr::logging::debug("Parsing entity {}", elementName);

        sr::model::Mesh<double, 3, 2> mesh;
        this->convertIFCtoMesh(geomObject, mesh);
        if (mesh.numPoints() == 0)
        {
            sr::logging::debug("Element has no points or valid geometry. Skipping.");
            continue;
        }

        if (skipElement(geomObject, mesh))
        {
            continue;
        }

        model_parser::ifc_parser::Element element =
          model_parser::ifc_parser::Element(geomObject->type(), entityGUID, mesh);
        sr::logging::debug("Element initialization succeded.");

        IfcElement* ifcElement = this->m_file->entityByGuid(entityGUID.string())->as<IfcElement>();

        this->extractMetadata(entityGUID, element.getMetadata());

        t_elements.push_back(element);
        this->m_progress = int(100 * float(n) / this->m_nAssignedElements);

    } while (contextIterator.next());
    this->m_elementsDone = true;
    this->m_progress = 100;
}

void IfcFile::retrievePSets(IfcSchema::IfcObject* object, errordetection_types::ElementMetadata& metadata)
{
    // This needs to be `auto`, or IFCV4 doesn't work
    auto definitions = object->IsDefinedBy();
    IfcSchema::IfcRelDefinesByProperties* byProp;
    IfcSchema::IfcPropertySet* pset;

    for (const auto& relationshipDefinition : *definitions)
    {
        // Retrieve only the IfcRelDefinesByProperties (subclass of IfcRelDefines)
        if ((byProp = relationshipDefinition->as<IfcSchema::IfcRelDefinesByProperties>()))
        {
            // Retrieve the property set
            if ((pset = byProp->RelatingPropertyDefinition()->as<IfcSchema::IfcPropertySet>()))
            {

                IfcProperty::list::ptr pSetProperties = pset->HasProperties();
                sr::logging::debug(
                  "Property set {}: {} properties found", pset->Name(), pSetProperties->size());

                std::map<std::string, std::string> propertySetProperties;
                for (const auto& property : *pSetProperties)
                {
                    IfcPropertySingleValue* pProp = property->as<IfcPropertySingleValue>();
                    if (pProp != nullptr)
                    {
                        if (pProp->hasNominalValue())
                        {
                            IfcValue* value = pProp->NominalValue();
                            auto attr = value->getArgument(0);
                            IfcUtil::ArgumentType type = attr->type();
                            switch (type)
                            {
                                case IfcUtil::Argument_INT:
                                case IfcUtil::Argument_BINARY:
                                case IfcUtil::Argument_DOUBLE:
                                case IfcUtil::Argument_STRING:
                                case IfcUtil::Argument_ENUMERATION:
                                case IfcUtil::Argument_ENTITY_INSTANCE:
                                    propertySetProperties[pProp->Name()] = (*attr).toString();
                                    break;
                                case IfcUtil::Argument_BOOL:
                                    propertySetProperties[pProp->Name()] =
                                      ((*attr).toString() == ".T.") ? "TRUE" : "FALSE";
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
                metadata.pset[pset->Name()] = propertySetProperties;
            }
        }
    }
}

void IfcFile::retrieveAttributes(
  IfcSchema::IfcObject* object,
  errordetection_types::ElementMetadata& metadata)
{
    IfcElement* ifcElement = object->as<IfcElement>();
    if (ifcElement == nullptr)
    {
        return;
    }

    for (int i = 0; i < ifcElement->getArgumentCount(); i++)
    {
        std::string key = ifcElement->getArgumentName(i);
        std::string value = ifcElement->getArgument(i)->toString();
        metadata.attribute.emplace(key, value);
    }
}

bool IfcFile::skipElement(
  const IfcGeom::Element<double>* geomObject,
  const sr::model::Mesh<double, 3, 2>& mesh)
{
    sr::logging::debug("Checking element {}-{} for filtering.", geomObject->type(), geomObject->guid());

    if (this->m_filters.minDiameterThreshold > 0)
    {
        pcl::PointCloud<pcl::PointXYZ> geomCloud;
        for (auto point : mesh.points())
        {
            pcl::PointXYZ p;
            p.getVector3fMap() = point.cast<float>();
            geomCloud.push_back(p);
        }

        double diameter = sr::cloud::ApproximateCloudDiameter(geomCloud);
        if (diameter < this->m_filters.minDiameterThreshold)
        {
            sr::logging::debug(
              "Element {} filtered due to diameter: {:2f}, min {:2f}",
              geomObject->guid(),
              diameter,
              this->m_filters.minDiameterThreshold);

            return true;
        }
    }

    if (this->m_filters.minAreaThreshold > 0)
    {
        double surfaceArea = sr::model::surfaceArea(mesh);
        if (surfaceArea < this->m_filters.minAreaThreshold)
        {
            sr::logging::debug(
              "Element {} filtered due to surface are: {:2f}, min {:2f}",
              geomObject->guid(),
              surfaceArea,
              this->m_filters.minAreaThreshold);

            return true;
        }
    }

    return false;
}

void IfcFile::getTransparentMaterialsIds()
{

// NOTE API between IFC2x3 and IFC4 schemas for querying transparent-styled elements appears to be different.
// We only support transparent element querying in IFC2x3
#ifndef USE_IFC4

    IfcMaterialDefinitionRepresentation::list::ptr matDefReps =
      m_file->entitiesByType<IfcMaterialDefinitionRepresentation>();
    for (const auto& matDefRep : *matDefReps)
    {
        IfcMaterial* mat = matDefRep->RepresentedMaterial();

        // matDefRepIt is an IFCMaterialDefinitionRepresentation
        IfcRepresentation::list::ptr representations = matDefRep->Representations();
        for (const auto& representation : *representations)
        {
            // representation is an IFCStyledRepresentation
            IfcRepresentationItem::list::ptr representationItems = representation->Items();
            for (const auto& item : *representationItems)
            {
                // item is an IfcRepresentationItem
                IfcStyledItem* styledItem = item->as<IfcStyledItem>();
                IfcPresentationStyleAssignment::list::ptr presentationStyleAssignments = styledItem->Styles();
                for (const auto& presentationStyleAssignment : *presentationStyleAssignments)
                {
                    IfcEntityList::ptr entities = presentationStyleAssignment->Styles();
                    for (const auto& surfaceStyleEntity : *entities)
                    {
                        // surfaceStyle is an IFCSurfaceStyle
                        IfcSurfaceStyle* surfaceStyle = surfaceStyleEntity->as<IfcSurfaceStyle>();
                        IfcEntityList::ptr surfaceStyleRenders = surfaceStyle->Styles();
                        for (const auto& surfaceStyleRender : *surfaceStyleRenders)
                        {
                            IfcSurfaceStyleRendering* surfacestyleRendering =
                              surfaceStyleRender->as<IfcSurfaceStyleRendering>();
                            if (!surfacestyleRendering->hasTransparency())
                            {
                                continue;
                            }
                            double transparency = surfacestyleRendering->Transparency();
                            if (transparency >= m_filters.transparentThreshold)
                            {
                                // The material is transparent
                                this->m_transparentMaterialsIds.push_back(mat->id());
                            }
                        }
                    }
                }
            }
        }
    }

#endif
}

bool IfcFile::hasTransparentMaterial(IfcProduct* product)
{
    bool hasTransparentMaterial = false;
    IfcRelAssociates::list::ptr relAssociates = product->HasAssociations();

    for (const auto& relAssociate : *relAssociates)
    {
        IfcRelAssociatesMaterial* associateMaterial = relAssociate->as<IfcRelAssociatesMaterial>();
        if (associateMaterial == nullptr)
        {
            continue;
        }
        else
        {
            IfcMaterialSelect* matSel = associateMaterial->RelatingMaterial();
            for (const auto& id : this->m_transparentMaterialsIds)
            {
                if (this->selectsMaterialWithID(matSel, id))
                {
                    // Product has a transparent material
                    return true;
                }
            }
        }
    }
    return hasTransparentMaterial;
}

bool IfcFile::selectsMaterialWithID(IfcMaterialSelect* materialSelect, int id) const
{
    bool tmp = false;
    if (materialSelect->type() == IfcMaterialLayerSetUsage::Class())
    {
        tmp = this->selectsMaterialWithID((materialSelect->as<IfcMaterialLayerSetUsage>()), id);
    }
    else if (materialSelect->type() == IfcMaterialLayerSet::Class())
    {
        tmp = this->selectsMaterialWithID((materialSelect->as<IfcMaterialLayerSet>()), id);
    }
    else if (materialSelect->type() == IfcMaterialLayer::Class())
    {
        tmp = this->selectsMaterialWithID((materialSelect->as<IfcMaterialLayer>()), id);
    }
    else if (materialSelect->type() == IfcMaterialList::Class())
    {
        tmp = this->selectsMaterialWithID((materialSelect->as<IfcMaterialList>()), id);
    }
    else if (materialSelect->type() == IfcMaterial::Class())
    {
        tmp = this->selectsMaterialWithID((materialSelect->as<IfcMaterial>()), id);
    }
    else
    {
        throw std::runtime_error(
          "IfcMaterialSelect child instance not recognized: " +
          IfcSchema::Type::ToString(materialSelect->type()));
    }
    return tmp;
}

bool IfcFile::selectsMaterialWithID(IfcMaterialLayerSetUsage* materialLayerSetUsage, int id) const
{
    return selectsMaterialWithID(materialLayerSetUsage->ForLayerSet(), id);
}

bool IfcFile::selectsMaterialWithID(IfcMaterialLayerSet* materialLayerSet, int id) const
{
    bool res = true;
    IfcMaterialLayer::list::ptr materialLayers = materialLayerSet->MaterialLayers();
    for (const auto& materialLayer : *materialLayers)
    {
        res = res && selectsMaterialWithID(materialLayer, id);
    }
    return res;
}

bool IfcFile::selectsMaterialWithID(IfcMaterialLayer* materialLayer, int id) const
{
    return selectsMaterialWithID(materialLayer->Material(), id);
}

bool IfcFile::selectsMaterialWithID(IfcMaterial* mat, int id) const
{
    return (id == mat->id());
}

bool IfcFile::selectsMaterialWithID(IfcMaterialList* materialList, int id) const
{
    IfcMaterial::list::ptr materials = materialList->Materials();
    for (const auto& material : *materials)
    {
        if (this->selectsMaterialWithID(material, id))
        {
            return true;
        }
    }
    return false;
}

void IfcFile::createFilters()
{

    // Filters
    std::string filterName;

    filterName = "shards";
    if (this->m_filters.nThreads > 1)
    {
        // Shard filter for pseudo-parallelizing the code. For num_shards == 1 returns true always.
        this->m_IFCfilters.push_back([this, filterName](IfcSchema::IfcProduct* x) {
            std::hash<std::string> hash;
            auto shard = hash(x->GlobalId()) % this->m_filters.nThreads;
            bool filter = shard == this->m_filters.shard;
            return filter;
        });
        sr::logging::debug("[Filter] {} filter ON", filterName);
    }
    else
    {
        sr::logging::debug("[Filter] {} filter OFF", filterName);
    }

    // Element is an IFCElement: In Ifc2x3 only IfcElements and children have geometry
    filterName = "IsIfcElement";
    this->m_IFCfilters.push_back([this, filterName](IfcSchema::IfcProduct* x) {
        bool passesFilter;
        IfcElement* pElement = x->as<IfcElement>();
        passesFilter = pElement != nullptr;
        if (!passesFilter)
        {
            sr::logging::debug("[Filter] Element {} filtered at filter {}", x->GlobalId(), filterName);
        }
        return passesFilter;
    });
    sr::logging::debug("[Filter] {} filter ON", filterName);

    // Type of element not in list of types of elements to filter (IFCWALL,IFCDOOR,etc)
    filterName = "ElementType";
    if (this->m_filters.filterElements)
    {
        this->m_IFCfilters.push_back([this, filterName](IfcSchema::IfcProduct* x) {
            bool passesFilter;
            passesFilter = std::find(
                             this->m_filters.elementsToFilter.begin(),
                             this->m_filters.elementsToFilter.end(),
                             IfcSchema::Type::ToString(x->type())) == this->m_filters.elementsToFilter.end();
            if (!passesFilter)
            {
                sr::logging::debug("[Filter] Element {} filtered at filter {}", x->GlobalId(), filterName);
            }
            return passesFilter;
        });
        sr::logging::debug("[Filter] {} filter ON", filterName);
    }
    else
    {
        sr::logging::debug("[Filter] {} filter OFF", filterName);
    }

    filterName = "Transparency";
    if (this->m_filters.filterTransparent)
    {
        // Element has a transparent material
        this->getTransparentMaterialsIds();
        this->m_IFCfilters.push_back([this, filterName](IfcSchema::IfcProduct* x) {
            bool passesFilter;
            passesFilter = !this->hasTransparentMaterial(x);
            if (!passesFilter)
            {
                sr::logging::debug("[Filter] Element {} filtered at filter {}", x->GlobalId(), filterName);
            }
            return passesFilter;
        });
        sr::logging::debug("[Filter] {} filter ON", filterName);
    }
    else
    {
        sr::logging::debug("[Filter] {} filter OFF", filterName);
    }

    filterName = "GUIDFilter";
    if (this->m_filters.filterByGuid)
    {
        std::vector filterGuids = this->m_filters.guidsToFilter;
        this->m_IFCfilters.push_back([this, filterName, filterGuids](IfcSchema::IfcProduct* x) {
            bool passesFilter;
            passesFilter =
              std::find(filterGuids.begin(), filterGuids.end(), x->GlobalId()) == filterGuids.end();
            if (!passesFilter)
            {
                sr::logging::debug("[Filter] Element {} filtered at filter {}", x->GlobalId(), filterName);
            }
            return passesFilter;
        });
        sr::logging::debug("[Filter] {} filter ON", filterName);
    }
    else
    {
        sr::logging::debug("[Filter] {} filter OFF", filterName);
    }
}

template <typename T>
void IfcFile::convertIFCtoMesh(IfcGeom::Element<T>* t_geomObject, sr::model::Mesh<double, 3, 2>& t_mesh)
{
    const IfcGeom::TriangulationElement<double>* triangulation =
      static_cast<const IfcGeom::TriangulationElement<double>*>(t_geomObject);
    const IfcGeom::Representation::Triangulation<double>& mesh = triangulation->geometry();

    assert(mesh.verts().size() % 3 == 0);
    assert(mesh.faces().size() % 3 == 0);

    std::size_t nPoints = mesh.verts().size() / 3;
    std::size_t nFaces = mesh.faces().size() / 3;
    t_mesh.resize(nPoints, nFaces);

    const std::vector<double>& verts = mesh.verts();
    for (std::size_t v = 0; v < nPoints; v++)
    {
        double x = verts[3 * v];
        double y = verts[3 * v + 1];
        double z = verts[3 * v + 2];

        t_mesh.point(v) = Eigen::Matrix<double, 3, 1>(x, y, z);
    }
    sr::logging::debug("{} vertices added.", nPoints);

    const std::vector<int>& faces = mesh.faces();
    for (std::size_t f = 0; f < nFaces; f++)
    {
        Eigen::Index f0 = faces[3 * f];
        Eigen::Index f1 = faces[3 * f + 1];
        Eigen::Index f2 = faces[3 * f + 2];

        assert(f0 >= 0 && f0 < nPoints);
        assert(f1 >= 0 && f1 < nPoints);
        assert(f2 >= 0 && f2 < nPoints);

        t_mesh.face(f) = Eigen::Matrix<Eigen::Index, 3, 1>(f0, f1, f2);
    }
    sr::logging::debug("{} faces added.", nFaces);
    // Remove duplicate points/faces and degenerate faces in the geometry
}

void IfcFile::getAssembledElements(std::vector<model_parser::ifc_parser::Assembly>& assemblies)
{
    assemblies.clear();
    IfcRelAggregates::list::ptr aggregations = m_file->entitiesByType<IfcRelAggregates>();
    for (const auto& aggregation : *aggregations)
    {
        if (aggregation->RelatingObject()->type() == IfcElementAssembly::Class())
        {
            std::string assemblyGuid = aggregation->RelatingObject()->GlobalId();

            errordetection_types::ElementMetadata metadata;
            model_parser::ifc_parser::Assembly assembly(metadata);
            assembly.getMetadata().elementType =
              errordetection_types::toString(errordetection_types::ElementType::IfcElementAssembly);
            this->extractMetadata(assemblyGuid, assembly.getMetadata());

            IfcObjectDefinition::list::ptr relatedObjects = aggregation->RelatedObjects();
            if (relatedObjects->size() > 0)
            {
                for (auto object : *relatedObjects)
                {
                    assembly.addElement(object->GlobalId());
                }
            }
            if (assembly.getElements().size() > 0)
            {
                assemblies.push_back(assembly);
            }
        }
    }
}

void IfcFile::extractMetadata(const sr::model::GUID& guid, errordetection_types::ElementMetadata& metadata)
{
    IfcSchema::IfcObject* object = m_file->entityByGuid(guid.string())->as<IfcSchema::IfcObject>();

    metadata.guid = guid;

    this->retrievePSets(object, metadata);
    this->retrieveAttributes(object, metadata);
}

} // namespace model_parser::ifc_parser
