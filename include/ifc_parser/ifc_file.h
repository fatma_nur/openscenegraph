#pragma once

#include <atomic>
#include <stdio.h>
#include <stdlib.h>
#include <string>

#include <boost/algorithm/string/split.hpp>
#include <boost/filesystem.hpp>

#include <ifcgeom/IfcGeom.h>
#include <ifcgeom/IfcGeomIterator.h>
#include <ifcgeom/IfcGeomIteratorSettings.h>
#include <ifcparse/IfcBaseClass.h>
#include <ifcparse/IfcHierarchyHelper.h>
#include <ifcparse/IfcLogger.h>

#include <gp_Trsf.hxx>
#include <gp_XYZ.hxx>

#include <fmt/format.h>

#include "sr/model/guid.h"
#include "sr/model/mesh.h"

#include "ifc_parser/assembly.h"
#include "ifc_parser/element.h"

namespace model_parser::ifc_parser
{

struct IfcFileFilters
{
    std::size_t nThreads = 1;
    std::size_t shard = 0;
    bool filterTransparent = false;
    double transparentThreshold = 0.7;
    double minDiameterThreshold = 0.05;
    double minAreaThreshold = 0.01;
    bool filterElements = false;
    std::vector<std::string> elementsToFilter;
    bool filterByGuid = false;
    std::vector<sr::model::GUID> guidsToFilter;
};

class IfcFile
{
public:
    typedef IfcFile Self;

    /** Constructor with boost path.*/
    inline IfcFile(boost::filesystem::path t_file, IfcFileFilters filters = IfcFileFilters());

    /** Copy Constructor */
    inline IfcFile(const Self&);

    /** File path getter.*/
    inline boost::filesystem::path getFilePath() const;

    /** Reads the IFC file and preprocesses it to clean it. */
    inline bool read();

    /** Obtains all the geometrical elements from the IFC file */
    void populateElements(std::vector<model_parser::ifc_parser::Element>& t_elements);

    /** Shard idx getter */
    inline std::size_t getIdxShard() const;

    /** num shards getter */
    inline std::size_t getNumShards() const;

    /** elements done getter */
    inline bool getElementsDone() const;

    // Number of assigned elements getter */
    inline size_t getNumberOfAssignedElements() const;

    /** Progress getter */
    inline int getProgress() const;

    /** Retrieves a list of ifc assemblies from the file.
     * \param[in] assemblies list to be filled with assemblies. It will be cleared and then filled.
     */
    inline void getAssembledElements(std::vector<model_parser::ifc_parser::Assembly>& assemblies);

    /** Retrieves metadata for an element */
    inline void extractMetadata(const sr::model::GUID& guid, errordetection_types::ElementMetadata& metadata);

private:
    IfcFileFilters m_filters;                    /** Filters settings*/
    boost::shared_ptr<IfcParse::IfcFile> m_file; /** IFC file parser class */
    boost::filesystem::path m_filePath;          /** IFC file path */
    std::size_t m_nAssignedElements;             /** Number of elements this object has to process */
    std::vector<IfcGeom::filter_t> m_IFCfilters; /** List of filters to apply to the geometry iterator */
    std::vector<int> m_transparentMaterialsIds;  /** List of transparent materials */
    bool m_elementsDone;

    std::atomic<int> m_progress;

    template <class T>
    IfcGeom::Element<T>* getGeometry(IfcSchema::IfcRepresentation* t_representation);

    inline void retrievePSets(IfcSchema::IfcObject* object, errordetection_types::ElementMetadata& metadata);

    inline void retrieveAttributes(
      IfcSchema::IfcObject* object,
      errordetection_types::ElementMetadata& metadata);

    inline bool skipElement(
      const IfcGeom::Element<double>* geomObject,
      const sr::model::Mesh<double, 3, 2>& mesh);

    /** Filters transparent elements from the IFC file according to the settings. */
    inline void getTransparentMaterialsIds();
    /** Checks if an IfcMaterialSelect is on a transparent element */
    inline bool selectsMaterialWithID(IfcSchema::IfcMaterialSelect* materialSelect, int id) const;
    /** Checks if an IfcMaterialLayerSetUsage is on a transparent element */
    inline bool selectsMaterialWithID(IfcSchema::IfcMaterialLayerSetUsage* materialLayerSetUsage, int id)
      const;
    /** Checks if an IfcMaterialLayerSet is on a transparent element */
    inline bool selectsMaterialWithID(IfcSchema::IfcMaterialLayerSet* materialLayerSet, int id) const;
    /** Checks if an IfcMaterialLayer is on a transparent element */
    inline bool selectsMaterialWithID(IfcSchema::IfcMaterialLayer* materialLayer, int id) const;
    /** Checks if an IfcMaterialList is on a transparent element */
    inline bool selectsMaterialWithID(IfcSchema::IfcMaterialList* matList, int id) const;
    /** Checks if an IfcMaterial is on a transparent element */
    inline bool selectsMaterialWithID(IfcSchema::IfcMaterial* mat, int id) const;
    /** Convert an IFC geometry into a sr::model::Mesh*/
    template <class T>
    void convertIFCtoMesh(IfcGeom::Element<T>* t_geomObject, sr::model::Mesh<double, 3, 2>& t_mesh);
    /** Checks whether an IfcProduct has one of the transparent material ids at m_transparentMaterialsIds */
    inline bool hasTransparentMaterial(IfcSchema::IfcProduct* product);
    /** Create the file filters */
    inline void createFilters();
};

} // namespace model_parser::ifc_parser

#include "ifc_parser/ifc_file.hpp"
