/*
 * Copyright (c) 2020, Scaled Robotics SL
 * License : http://www.binpress.com/license/view/l/8ef75db882b3f91ba42b114142f3e8b3
 */

/** \file */

#pragma once

#include <algorithm>
#include <fstream>
#include <iostream>
#include <map>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>

#include <boost/filesystem.hpp>

#include "sr/filesystem.h"
#include "sr/math/box.h"
#include "sr/model/guid.h"
#include "sr/model/mesh.h"

#include <Eigen/Core>

#include "errordetection_types/element_metadata.h"
#include "errordetection_types/element_type.h"

namespace model_parser::ifc_parser
{
class Element
{

public:
    /** Constructor from a model mesh*/
    inline Element(std::string t_type, sr::model::GUID t_guid, sr::model::Mesh<double, 3, 2>& t_geometry);

    /** GUID getter */
    inline sr::model::GUID getGUID() const;
    /** Element type getter */
    inline std::string getElementType() const;
    /** Element name getter */
    inline std::string getElementName() const;
    /** Geometry getter */
    inline const sr::model::Mesh<double, 3, 2>& getGeometry() const;

    /** Metadata getter */
    inline errordetection_types::ElementMetadata& getMetadata();

    inline errordetection_types::ElementMetadata getMetadata() const;

private:
    /** Bounding box getter */
    inline std::array<double, 6> getBoundingBox(sr::model::Mesh<double, 3, 2>& model) const;

    sr::model::GUID m_GUID;                           /**< Global Unique IDentifier from the IFC*/
    sr::model::Mesh<double, 3, 2> m_geometry;         /**< Mesh class to hold the geometry */
    std::string m_elementType;                        /**< IFC element type */
    std::string m_elementName;                        /**< Element name  */
    errordetection_types::ElementMetadata m_metadata; /**< Element metadata */
};

} // namespace model_parser::ifc_parser::

#include "ifc_parser/element.hpp"
