/*
 * Copyright (c) 2020, Scaled Robotics SL
 * License : http://www.binpress.com/license/view/l/8ef75db882b3f91ba42b114142f3e8b3
 */

/** \file */

#pragma once

#include <iostream>
#include <string>

#include "sr/model/guid.h"

#include "errordetection_types/element_metadata.h"

namespace model_parser::ifc_parser
{
class Assembly
{

public:
    inline Assembly(const errordetection_types::ElementMetadata t_metadata);

    inline sr::model::GUID getGUID() const;

    inline errordetection_types::ElementMetadata& getMetadata();

    inline const errordetection_types::ElementMetadata& getMetadata() const;

    inline void addElement(const sr::model::GUID& guid);

    inline std::vector<sr::model::GUID>& getElements();

    inline const std::vector<sr::model::GUID>& getElements() const;

private:
    errordetection_types::ElementMetadata m_metadata; /**< Assembly metadata */
    std::vector<sr::model::GUID> m_elementGUIDs;
};

} // namespace model_parser::ifc_parser::

#include "ifc_parser/assembly.hpp"
