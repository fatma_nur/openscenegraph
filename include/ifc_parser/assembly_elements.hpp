/*
 * Copyright (c) 2020, Scaled Robotics SL
 * License : http://www.binpress.com/license/view/l/8ef75db882b3f91ba42b114142f3e8b3
 */

/** \file */

#pragma once

#include <fmt/format.h>

#include "ifc_parser/element.h"
#include "ifc_parser/ifc_file.h"

#include "ifc_parser/assembly_elements.h"

model_parser::ifc_parser::AssemblyElements::AssemblyElements() {}

model_parser::ifc_parser::AssemblyElements::AssemblyElements(
  const errordetection_types::ElementMetadata& t_metadata)
{
    this->m_metadata = t_metadata;

    this->m_metadata.bbox = {
        std::numeric_limits<float>::infinity(), -std::numeric_limits<float>::infinity(),
        std::numeric_limits<float>::infinity(), -std::numeric_limits<float>::infinity(),
        std::numeric_limits<float>::infinity(), -std::numeric_limits<float>::infinity()
    };
}

model_parser::ifc_parser::AssemblyElements::AssemblyElements(const sr::model::GUID& guid)
{
    this->m_metadata.guid = guid;
}

sr::model::GUID model_parser::ifc_parser::AssemblyElements::getGUID() const
{
    if (this->m_elements.size() == 1)
    {
        return this->m_elements[0].getMetadata().guid;
    }
    else
    {
        return this->m_metadata.guid;
    }
}

sr::model::Mesh<double, 3, 2> model_parser::ifc_parser::AssemblyElements::getGeometry() const
{
    if (this->m_elements.size() == 1)
    {
        return this->m_elements[0].getGeometry();
    }
    else
    {
        sr::model::Mesh<double, 3, 2> ret;
        for (auto element : this->m_elements)
        {
            ret = ret + element.getGeometry();
        }
        return ret;
    }
}

errordetection_types::ElementMetadata model_parser::ifc_parser::AssemblyElements::getMetadata() const
{
    if (this->m_elements.size() == 1)
    {
        return this->m_elements[0].getMetadata();
    }
    else
    {
        return this->m_metadata;
    }
}

void model_parser::ifc_parser::AssemblyElements::addElement(const model_parser::ifc_parser::Element& element)
{
    this->m_elements.push_back(element);
    // Update assembly bbox
    auto elementBox = element.getMetadata().bbox;
    // We compute the minimum bounding box that contains both m_metadata.bbox and elementBox.
    // Remember, bbox entries are: minX, maxX, minY, maxY, minZ, maxZ
    for (int idx : { 0, 1, 2 })
    {
        int minIdx = 2 * idx;
        int maxIdx = 1 + minIdx;
        this->m_metadata.bbox[minIdx] = std::min<double>(this->m_metadata.bbox[minIdx], elementBox[minIdx]);
        this->m_metadata.bbox[maxIdx] = std::max<double>(this->m_metadata.bbox[maxIdx], elementBox[maxIdx]);
    }
}

std::vector<model_parser::ifc_parser::Element>& model_parser::ifc_parser::AssemblyElements::getElements()
{
    return this->m_elements;
}

std::map<sr::model::GUID, model_parser::ifc_parser::AssemblyElements>
model_parser::ifc_parser::mergeAssemblyElements(
  const std::vector<boost::filesystem::path>& inputFiles,
  std::map<sr::model::GUID, model_parser::ifc_parser::AssemblyElements>& elementList)
{

    // Accumulate map of all assembled elements
    std::vector<model_parser::ifc_parser::Assembly> assemblies;
    for (auto& inputFile : inputFiles)
    {
        model_parser::ifc_parser::IfcFile ifcFile = model_parser::ifc_parser::IfcFile(inputFile);
        ifcFile.read();
        std::vector<model_parser::ifc_parser::Assembly> fileAssemblies;
        ifcFile.getAssembledElements(fileAssemblies);

        assemblies.insert(assemblies.end(), fileAssemblies.begin(), fileAssemblies.end());
    }

    // Extract all the assembly GUIDs
    std::vector<sr::model::GUID> guidsInAssemblies;

    for (const auto& assembly : assemblies)
    {
        for (auto b : assembly.getElements())
        {
            guidsInAssemblies.push_back(b);
        }
    }
    std::sort(guidsInAssemblies.begin(), guidsInAssemblies.end());

    // First of all add all elements not in an assembly
    std::map<sr::model::GUID, model_parser::ifc_parser::AssemblyElements> mergedElements;
    for (const auto& [guid, element] : elementList)
    {
        if (!std::binary_search(guidsInAssemblies.begin(), guidsInAssemblies.end(), guid))
        {
            mergedElements[guid] = element;
        }
    }

    // Then append the assemblies as a single merged element
    for (const auto& assembly : assemblies)
    {
        sr::model::GUID assemblyGUID = assembly.getGUID();
        auto assemblyMetadata = assembly.getMetadata();

        model_parser::ifc_parser::AssemblyElements assemblyElements(assemblyMetadata);
        for (auto& guid : assembly.getElements())
        {
            auto els = elementList[guid].getElements();
            if (els.size() == 0)
            {
                continue;
            }
            assemblyElements.addElement(els[0]);
        }

        if (assemblyElements.getElements().size() == 0)
        {
            continue;
        }
        mergedElements[assemblyGUID] = assemblyElements;
    }

    return mergedElements;
}
