#pragma once

#include <iostream>
#include <regex>

#include <boost/algorithm/string.hpp>
#include <boost/optional.hpp>
#include <boost/property_tree/ptree.hpp>

#include "sr/model/guid.h"

#include "errordetection_types/element_metadata.h"

class MetadataParser
{
public:
    /** ID from the .obj subgroups in the total .obj retrieved with model derivative, after sanitizing it,
     * i.e. g Obj.42:32 becomes 42*/
    typedef std::string ObjectID;

    /**
     * Constructor with path
     * \param[in] t_metadataFile metadata file from the forge model derivative process
     */
    MetadataParser(const boost::property_tree::ptree& metadata) { m_metadata = metadata; }

    // ~MetadataParser not implemented on purpose

    /**
     *  Builds a map from the metadata where the names in the .obj are related to unique GUIDs
     *  \param[out] t_nameGuidMap Unordered map with the .obj subgroup names as keys and element GUIDs as
     * values. \param[out] t_srMetadata Vector of elements metadata with the same structure as the ifc_parser
     */
    virtual void getNameGUIDMap(
      std::unordered_map<std::string, sr::model::GUID>& nameGuidMap,
      std::vector<errordetection_types::ElementMetadata>& srMetadata) const = 0;

    /**
     * Map element type read in the metadata to a common type that we use internally
     * eg 'IFCWALL' (IFC) = 'Revit Walls' (Revit) = 'Walls' (Navis)
     */
    virtual std::string mapElementType(std::string elementType) const = 0;

protected:
    boost::property_tree::ptree m_metadata;
};
