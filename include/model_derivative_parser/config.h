#pragma once

#include "sr/io/versioned_config.h"
#include "sr/reflection/struct.h"
#include "sr/util/const_value.h"

namespace model_parser::model_derivative_parser
{
template <int Version = 0>
struct ModelDerivativeParserConfig;

template <>
struct ModelDerivativeParserConfig<0> : sr::io::VersionedConfig
{
    sr::util::ConstValue<int> version{0};
    std::string extension = ".obj";
};
} // namespace model_parser::model_derivative_parser

// clang-format off
SR_REFLECTION_REGISTER_STRUCT(model_parser::model_derivative_parser::ModelDerivativeParserConfig<0>, "ModelDerivativeParserConfig",
    (version,   "version",   "Config version 0")
    (extension, "extension", "Extension for output files")
)
// clang-format on
