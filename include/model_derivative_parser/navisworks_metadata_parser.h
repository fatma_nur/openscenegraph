#pragma once

#include "model_derivative_parser/metadata_tree_parser.h"

class NavisWorksMetadataParser : public MetadataTreeParser
{
public:
    /**
     * Constructor with json data tree
     * \param[in] t_metadata metadata structure from the forge model derivative process
     */
    inline NavisWorksMetadataParser(const boost::property_tree::ptree& metadata)
      : MetadataTreeParser(metadata)
    {
    }

    static bool isNavis(const boost::property_tree::ptree& metadata)
    {
        if (metadata.empty())
        {
            return false;
        }
        auto& modelFileMetadata = metadata.begin()->second;
        auto modelFile = modelFileMetadata.get_optional<std::string>(NameKey);
        return modelFile and (boost::filesystem::extension(*modelFile) == ".nwd" or
                              boost::filesystem::extension(*modelFile) == ".nwc");
    }

    /**
     * Map Navis type to a common internal type
     * To be decided, for now just return the Navis type
     */
    std::string mapElementType(std::string navisType) const override { return navisType; }
};
