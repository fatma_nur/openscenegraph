#pragma once
#include <cmath>

#include "model_derivative_parser/metadata_parser.h"

#include "sr/logging/log_writer.h"

#include <iostream>
#include <string>

class RevitMetadataParser : public MetadataParser
{
public:
    static const std::string ObjectIdKey;
    static const std::string GuidKey;
    static const std::string NameKey;
    static const std::string TypeKey;
    static const std::string DocumentSchemaKey;
    static const std::string DocumentSchemaName;

    /**
     * Constructor with json data tree
     * \param[in] metadata metadata structure from the forge model derivative process
     */
    RevitMetadataParser(const boost::property_tree::ptree& metadata)
      : MetadataParser(metadata)
    {
    }

    /**
     * The first metadata element contains file/schema information
     * Check that the schema information exists and is 'rvt'
     * "__document__": {
     *     "schema_name": "rvt",
     *     "schema_version": "1.0",
     *     "is_doc_property": 1
     *  }
     */
    static bool isRevit(const boost::property_tree::ptree& metadata)
    {
        if (metadata.empty())
        {
            return false;
        }
        auto& modelFileMetadata = metadata.begin()->second;
        auto revitSchema = modelFileMetadata.get_optional<std::string>(DocumentSchemaKey);
        return revitSchema and *revitSchema == DocumentSchemaName;
    }

    /**
     * Map Revit type to a common internal type
     * To be decided, for now just return the Revit type
     */
    std::string mapElementType(std::string revitType) const override { return revitType; }

    /**
     * For Revit metadata files, we simply parse each metadata object in turn
     * and check that it contains an objectid, guid, type and optionally a name
     */
    inline void getNameGUIDMap(
      std::unordered_map<std::string, sr::model::GUID>& nameGuidMap,
      std::vector<errordetection_types::ElementMetadata>& srMetadata) const override
    {
        sr::logging::info("Parsing {} metadata from Revit", m_metadata.size());

        nameGuidMap.clear();
        srMetadata.clear();
        srMetadata.reserve(m_metadata.size());

        for (auto iter = this->m_metadata.begin(); iter != this->m_metadata.end(); iter++)
        {
            auto& metadata = iter->second;

            auto objectid = metadata.get_optional<std::string>(ObjectIdKey);
            auto guid = metadata.get_optional<std::string>(GuidKey);
            auto name = metadata.get_optional<std::string>(NameKey);
            auto type = metadata.get_optional<std::string>(TypeKey);

            if (!objectid or !guid or !type)
            {
                continue;
            }

            nameGuidMap[*objectid] = *guid;

            errordetection_types::ElementMetadata srElementMetadata;
            srElementMetadata.guid = *guid;
            srElementMetadata.elementType = mapElementType(*type);
            if (name)
            {
                srElementMetadata.attribute["name"] = *name;
            }
            srMetadata.push_back(srElementMetadata);
        }

        sr::logging::info(
          "RevitMetadataParser: parsed {}/{} metadata elements", srMetadata.size(), m_metadata.size());
    }
};

inline const std::string RevitMetadataParser::ObjectIdKey = "objectid";
inline const std::string RevitMetadataParser::GuidKey = "externalId";
inline const std::string RevitMetadataParser::NameKey = "__name__.name";
inline const std::string RevitMetadataParser::TypeKey = "__category__.Category";
inline const std::string RevitMetadataParser::DocumentSchemaKey = "__document__.schema_name";
inline const std::string RevitMetadataParser::DocumentSchemaName = "rvt";