#pragma once

#include "model_derivative_parser/metadata_tree_parser.h"

class IfcMetadataParser : public MetadataTreeParser
{
public:
    IfcMetadataParser(const boost::property_tree::ptree& metadata)
      : MetadataTreeParser(metadata)
    {
    }

    static bool isIFC(const boost::property_tree::ptree& metadata)
    {
        if (metadata.empty())
        {
            return false;
        }
        auto& modelFileMetadata = metadata.begin()->second;
        auto modelFile = modelFileMetadata.get_optional<std::string>(NameKey);
        return modelFile and boost::filesystem::extension(*modelFile) == ".ifc";
    }

    /**
     * Map IFC type to a common internal type
     * To be decided, for now just return the IFC type
     */
    std::string mapElementType(std::string ifcType) const override { return ifcType; }
};
