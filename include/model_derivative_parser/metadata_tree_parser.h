#pragma once

#include "model_derivative_parser/metadata_parser.h"

#include "sr/logging/log_writer.h"

class MetadataTreeParser : public MetadataParser
{
public:
    static const std::string ObjectIdKey;
    static const std::string ExternalIdKey;
    static const std::string GuidKey;
    static const std::string NameKey;
    static const std::string TypeKey;

    /** For IFC and Navis models, the external ID conveys the tree structure
     * eg. externalId=3/15/1/0/16/0/0 has parent 3/15/1/0/16/0 -> 3/15/1/0/16 -> etc */
    typedef std::string ExternalID;

    MetadataTreeParser(const boost::property_tree::ptree& metadata)
      : MetadataParser(metadata)
    {
    }

    /**
     *  Builds a map from the metadata where the names in the .obj are related to unique GUIDs
     * \param[out] nameGuidMap Unordered map with the .obj subgroup names as keys and element GUIDs as
     * values.
     * \param[out] srMetadata Vector of elements metadata with the same structure as the ifc_parser
     */
    void getNameGUIDMap(
      std::unordered_map<std::string, sr::model::GUID>& nameGuidMap,
      std::vector<errordetection_types::ElementMetadata>& srMetadata) const override
    {
        nameGuidMap.clear();
        srMetadata.clear();
        srMetadata.reserve(m_metadata.size());

        std::unordered_map<ObjectID, ExternalID> objectIdExternalIdMap;
        std::unordered_map<ExternalID, const boost::property_tree::ptree*> externalIdMetadataMap;

        size_t invalidMetadata = 0;

        for (auto iter = this->m_metadata.begin(); iter != this->m_metadata.end(); iter++)
        {
            const boost::property_tree::ptree& metadata = iter->second;

            auto objectId = metadata.get_optional<std::string>(ObjectIdKey);
            auto externalId = metadata.get_optional<std::string>(ExternalIdKey);

            if (!objectId or !externalId)
            {
                ++invalidMetadata;
                continue;
            }

            objectIdExternalIdMap[*objectId] = *externalId;
            externalIdMetadataMap[*externalId] = &metadata;
        }

        if (invalidMetadata > 0)
        {
            sr::logging::warning(
              "Skipping {} metadata objects without objectid or externalId", invalidMetadata);
        }

        std::set<std::string> metadataGuids;

        for (auto [objectId, externalId] : objectIdExternalIdMap)
        {
            while (true)
            {
                auto* metadata = externalIdMetadataMap.at(externalId);

                auto guid = metadata->get_optional<std::string>(GuidKey);
                auto name = metadata->get_optional<std::string>(NameKey);
                auto type = metadata->get_optional<std::string>(TypeKey);

                if (guid and name and type)
                {
                    nameGuidMap[objectId] = *guid;
                    if (metadataGuids.find(*guid) == metadataGuids.end())
                    {
                        errordetection_types::ElementMetadata srElementMetadata;
                        srElementMetadata.guid = *guid;
                        srElementMetadata.elementType = mapElementType(*type);
                        srElementMetadata.attribute["name"] = *name;
                        srMetadata.push_back(srElementMetadata);
                        metadataGuids.insert(*guid);
                    }
                    break;
                }
                else if (
                  !getParentId(externalId, externalId) or
                  externalIdMetadataMap.find(externalId) == externalIdMetadataMap.end())
                {
                    // If have reached the end of the tree of external IDs (no parent ID)
                    // or if there is no metadata matching the parent ID, then there is
                    // no metadata for this object and it will not be used
                    break;
                }
            }
        }

        sr::logging::info("Parsed {}/{} metadata elements", nameGuidMap.size(), objectIdExternalIdMap.size());
    }

    /**
     * Get parent ID for a given external ID
     * eg 3/15/1/0/16/0/0 -> 3/15/1/0/16/0
     * returns false if no parent exists
     */
    static bool getParentId(ExternalID externalId, ExternalID& parentId)
    {
        std::vector<std::string> tokens;
        boost::split(tokens, externalId, boost::is_any_of("/"));

        if (tokens.size() == 1)
        {
            return false; // no parent id
        }

        std::vector<std::string> parentTokens(tokens.begin(), tokens.end() - 1);
        parentId = boost::join(parentTokens, "/");
        return true;
    }
};

inline const std::string MetadataTreeParser::ObjectIdKey = "objectid";
inline const std::string MetadataTreeParser::ExternalIdKey = "externalId";
inline const std::string MetadataTreeParser::GuidKey = "Item.GUID";
inline const std::string MetadataTreeParser::NameKey = "Item.Name";
inline const std::string MetadataTreeParser::TypeKey = "Item.Type";