#include <osg/Group>
#include <osgDB/ReadFile>
#include <osgViewer/Viewer>
#include "sr/model/grouped_mesh.h"
#include "sr/math/random_matrix.h"
#include "sr/math/transform.h"
#include "sr/model/guid.h"
#include "sr/model/mesh.h"
#include "sr/test/scope_temporaldirectory.h"
#include <iostream>

int visualize_mesh();

int visualize_mesh()
{
  sr::model::GUID guid0("0$1qystJ59j8PjEM$RbhHp");
  sr::model::GUID guid1("0000000000000000000001");

  boost::filesystem::path meshPath("/home/fato/code/workspace/fato_fun_house/model/model_001/model.obj");
  boost::filesystem::path meshPath2("/home/fato/newmodel.obj");
  sr::model::GroupedMesh<double> readMesh;
  sr::model::GroupedMesh<double> newMesh;
  sr::model::Mesh<double,3,2> Mesh1;
  sr::model::readGroupedMesh<double>(meshPath, readMesh);

  Mesh1 = readMesh.getMesh(guid0);
  newMesh.addMesh(guid1, Mesh1);
  sr::model::writeGroupedMesh<double>(newMesh, meshPath2);

  osg::ref_ptr<osg::Group> root = new osg::Group;
  osg::Node* node = osgDB::readNodeFile("/home/fato/newmodel.obj");
  osg::Geode* g = dynamic_cast< osg::Geode*> (node->asGroup()->getChild(0));
  //osg::ref_ptr<osg::Node> cessnaNode = osgDB::readNodeFile("/home/fato/newmodel.obj");
  if (!g)
  {
      printf("Geode not created\n");
      return 1;
  }

  root ->addChild(g);

  osgViewer::Viewer viewer;
  viewer.setSceneData(root);
  return viewer.run();
}
