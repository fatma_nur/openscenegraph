#include <osg/Group>
#include <osg/Geometry>
#include <osg/BoundingBox>
#include <osgDB/ReadFile>
#include <osgViewer/Viewer>
#include <osg/MatrixTransform>
#include <osg/PrimitiveSet>
#include "sr/json/json_sink.h"
#include "sr/json/json_source.h"
#include "sr/model/grouped_mesh.h"
#include "sr/model/mesh.h"
#include "errordetection_types/element_metadata.h"
#include "errordetection_types/element_type.h"
#include "sr/test/scope_temporaldirectory.h"
#include "sr/model/guid.h"
#include <fstream>
#include <iostream>

void save_meshes(std::vector<errordetection_types::ElementMetadata> metadata);
void draw_bounding_box(osg::Geode* obj_g);
void mark_origin(osg::Geode* geode, osg::BoundingBox bb);

void save_meshes(std::vector<errordetection_types::ElementMetadata> metadata)
{
  boost::filesystem::path meshPath("/home/fato/code/workspace/fato_fun_house/model/model_001/model.obj");
  sr::model::Mesh<double,3,2> Mesh;
  sr::model::GroupedMesh<double> readMesh;
  sr::model::readGroupedMesh<double>(meshPath, readMesh);

  for(int i = 0; i < metadata.size(); i++)
  {
    sr::model::GroupedMesh<double> newMesh;
    boost::filesystem::path parsedMeshes("/home/fato/meshes/" + metadata[i].guid.string() + ".obj");
    Mesh = readMesh.getMesh(metadata[i].guid);
    newMesh.addMesh(metadata[i].guid, Mesh);
    sr::model::writeGroupedMesh<double>(newMesh, parsedMeshes);
  }
}

void draw_bounding_box(osg::Geode* geode, osg::BoundingBox bb)
{
  std::cout << "Object Bounding box center is: "<< bb.center().x() << " " << bb.center().y()  << " "<< bb.center().z() << std::endl;
  osg::Geometry* linesGeom = new osg::Geometry();

  osg::Vec3Array* vertices = new osg::Vec3Array(8);
  (*vertices)[0] = bb.corner(0);
  (*vertices)[1] = bb.corner(1);
  (*vertices)[2] = bb.corner(2);
  (*vertices)[3] = bb.corner(3);
  (*vertices)[4] = bb.corner(4);
  (*vertices)[5] = bb.corner(5);
  (*vertices)[6] = bb.corner(6);
  (*vertices)[7] = bb.corner(7);


  // pass the created vertex array to the points geometry object.
  linesGeom->setVertexArray(vertices);

  // set the colors as before, plus using the aobve
  osg::Vec4Array* colors = new osg::Vec4Array;
  colors->push_back(osg::Vec4(1.0f,1.0f,0.0f,1.0f));
  linesGeom->setColorArray(colors);
  linesGeom->setColorBinding(osg::Geometry::BIND_PER_VERTEX);

	GLushort indices[] = {
		0,1,
		0,4,
		0,2,
		1,5,

		1,3,
		3,7,
		3,2,
		2,6,

		6,7,
		4,5,
		4,6,
		5,7
	};
  // This time we simply use primitive, and hardwire the number
	// of coords to use since we know up front,
  linesGeom->addPrimitiveSet(new
      osg::DrawElementsUShort(osg::PrimitiveSet::LINES,24,indices));

	geode->addDrawable(linesGeom);
}

void mark_origin(osg::Geode* geode)
{
  osg::Geometry* originGeom = new osg::Geometry();

  osg::Vec3Array* vertices = new osg::Vec3Array(8);
  (*vertices)[0] = osg::Vec3(0.0f,0.0f,0.0f);
  (*vertices)[1] = osg::Vec3(0.0f,0.0f,1.0f);
  (*vertices)[2] = osg::Vec3(0.0f,1.0f,0.0f);
  (*vertices)[3] = osg::Vec3(0.0f,1.0f,1.0f);
  (*vertices)[4] = osg::Vec3(1.0f,0.0f,0.0f);
  (*vertices)[5] = osg::Vec3(1.0f,0.0f,1.0f);
  (*vertices)[6] = osg::Vec3(1.0f,1.0f,0.0f);
  (*vertices)[7] = osg::Vec3(1.0f,1.0f,1.0f);


  // pass the created vertex array to the points geometry object.
  originGeom->setVertexArray(vertices);

  osg::Vec4Array* colors = new osg::Vec4Array;
  colors->push_back(osg::Vec4(1.0f,1.0f,0.0f,1.0f));
  originGeom->setColorArray(colors);
  originGeom->setColorBinding(osg::Geometry::BIND_PER_VERTEX);

	GLushort indices[] = {
		0,1,
		0,4,
		0,2,
		1,5,

		1,3,
		3,7,
		3,2,
		2,6,

		6,7,
		4,5,
		4,6,
		5,7
	};
  // This time we simply use primitive, and hardwire the number
	// of coords to use since we know up front,
  originGeom->addPrimitiveSet(new
      osg::DrawElementsUShort(osg::PrimitiveSet::LINES,24,indices));

  geode->addDrawable(originGeom);
}

int main()
{

  boost::filesystem::path metadataFile("/home/fato/code/workspace/fato_fun_house/model/model_001/metadata.json");

  std::vector<errordetection_types::ElementMetadata> metadata;
  std::vector<std::string> elements;
  std::ifstream fd(metadataFile.c_str(), std::ios::in);
  sr::json::JsonSource{ fd } >> metadata;

  //std::cout <<  metadata.size() << std::endl;

  osg::ref_ptr<osg::Group> root = new osg::Group;

  //save_meshes(metadata); //save the meshes as seperate .obj files, this is done only once

  osg::Node* obj_node = osgDB::readNodeFile("/home/fato/code/workspace/src/osg_ws/test/mixer_scaled.obj");
  osg::Geode* obj_g = dynamic_cast< osg::Geode*> (obj_node->asGroup()->getChild(0));

  osg::ref_ptr<osg::MatrixTransform> mt = new osg::MatrixTransform;
  osg::Matrix matT;

  bool first_flag = true;
  for(int i = 0; i < metadata.size(); i++)
  {
    osg::Node* mesh_node = osgDB::readNodeFile("/home/fato/meshes/" + metadata[i].guid.string() + ".obj");
    osg::Geode* mesh_g = dynamic_cast< osg::Geode*> (mesh_node->asGroup()->getChild(0));

    if (!mesh_g)
    {
        std::cout << "Mesh Geode " << metadata[i].guid.string() << " is not created\n" << std::endl;
        return 1;
    }

    //root->addChild(mesh_g);

    elements.push_back(metadata[i].elementType);

    if (elements[i] == "IfcSlab")
    {
      if (first_flag)
      {
        //mark_origin(mesh_g);
        //draw_bounding_box(mesh_g);
        //draw_bounding_box(obj_g);
        osg::BoundingBox bb = mesh_g->getBoundingBox();
        matT.makeTranslate(bb.center().x(),bb.center().y(),bb.center().z());
        mt->setMatrix(matT);
        mt->addChild(obj_g);
        mesh_g->addChild(mt.get());
        first_flag = false;
        //std::cout << obj_g->getNumParents() << " parents" << std::endl;
      }
    }

    root->addChild(mesh_g);
    //newMesh.addMesh(metadata[i].guid, Mesh);
    //sr::model::writeGroupedMesh<double>(newMesh, parsedMeshes);
  }
  osgViewer::Viewer viewer;
  viewer.setSceneData(root);

  return viewer.run();
  //return 0;

}
