# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/fato/code/workspace/src/osg_ws/include;/usr/include;/usr/include/eigen3".split(';') if "/home/fato/code/workspace/src/osg_ws/include;/usr/include;/usr/include/eigen3" != "" else []
PROJECT_CATKIN_DEPENDS = "scaled_common;ifc_open_shell;fmt;nanoflann;range-v3;kdl_parser;urdf;pcl_ros;tinyobjloader".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "/usr/lib/x86_64-linux-gnu/libboost_system.so;/usr/lib/x86_64-linux-gnu/libboost_filesystem.so;/usr/lib/x86_64-linux-gnu/libboost_program_options.so".split(';') if "/usr/lib/x86_64-linux-gnu/libboost_system.so;/usr/lib/x86_64-linux-gnu/libboost_filesystem.so;/usr/lib/x86_64-linux-gnu/libboost_program_options.so" != "" else []
PROJECT_NAME = "osg"
PROJECT_SPACE_DIR = "/home/fato/code/workspace/src/osg_ws/build/devel"
PROJECT_VERSION = "1.0.0"
